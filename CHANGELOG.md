# Changelog

#### HdbAttributeCheck -1.3  (04/11/2019):
    Set device FAULT if an attribute is not managed.

#### HdbAttributeCheck -1.2  (16/03/2018):
    CheckedAttributes attribute added.

#### HdbAttributeCheck -1.1  (16/03/2018):
    Initial revision
