# WARNING !
### New device interface under development. Use last tag !!!!!!!

# Project HdbAttributeCheck

Maven Java project

This class is able to control an attribute list.
Its state will be fault if one of these attributes is not found in HDB since last archive period.

### WARNING !
The archive period checked to found a datum is the longest archive period of the attribute list (one HDB call).
It means: the best way is to have attributes with similar archive period.
And CheckPeriod property coherent with it.  

#### See client for more info:
[https://gitlab.esrf.fr/accelerators/System/HdbAttributeCheckGUI](https://gitlab.esrf.fr/accelerators/System/HdbAttributeCheckGUI)


## Cloning

```
git clone  git@gitlab.esrf.fr:accelerators/System/HdbAttributeCheck
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* JTango.jar

#### Toolchain Dependencies

* javac 7 or higher
* maven


### Build


Instructions on building the project.

```
cd HdbAttributeCheck
mvn package
```

