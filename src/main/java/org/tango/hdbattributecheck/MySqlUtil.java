//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// $Author$
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision$
//
//-======================================================================


package org.tango.hdbattributecheck;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.DeviceData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *	This class group many info and methods used with MySql Select
 *
 * @author verdier
 */

public class MySqlUtil {
     //===============================================================
    /**
     * Execute a SELECT command on TANGO database.
     *
     * @param command the command to be executed
     * @throws DevFailed in case of database server is not running
     *                   or in case of syntax error in command parameter.
     * @return the data found in TANGO database
     */
    //===============================================================
    private static MySqlData executeMySqlSelect(String command) throws DevFailed {

        DeviceData argIn = new DeviceData();
        argIn.insert(command);
        DeviceData argOut = ApiUtil.get_db_obj().command_inout("DbMySqlSelect", argIn);

        return new MySqlData(argOut.extractLongStringArray());
    }
    //===============================================================
    //===============================================================
    public static List<String> getDeviceListForClass(String className) throws DevFailed {
        String query = "SELECT name FROM device WHERE class=\"" + className + "\"";
        MySqlData mySqlData = executeMySqlSelect(query);

        List<String> list = new ArrayList<>();
        for (MySqlRow row : mySqlData) {
            list.add(row.get(0));
        }
        return list;
    }
    //===============================================================
    //===============================================================
    public static List<String[]> getDeviceProperties(String wildcard, String propertyName) throws DevFailed {
        String query = "SELECT device,value FROM property_device WHERE device LIKE \"" +
                wildcard + "\"  AND name=\"" + propertyName + "\"";
        System.out.println(query);
        MySqlData mySqlData = executeMySqlSelect(query);
        List<String[]> list = new ArrayList<>();
        for (MySqlRow row : mySqlData) {
            String deviceName = row.get(0);
            String value = row.get(1);
            list.add(new String[] { deviceName, value } );
        }
        return list;
    }
    //===============================================================
    //===============================================================







    //===============================================================
    //===============================================================
    private static class MySqlRow extends ArrayList<String> {
        //===========================================================
        private MySqlRow(List<String> list) {
            super();
            this.addAll(list);
        }
        //===========================================================
    }

    //===============================================================
    //===============================================================
    private static class MySqlData extends ArrayList<MySqlRow> {
        //===========================================================
        private MySqlData(DevVarLongStringArray lsa) {
            super();
            int nbRows = lsa.lvalue[lsa.lvalue.length - 2];
            int nbFields = lsa.lvalue[lsa.lvalue.length - 1];
            int idx = 0;
            for (int i = 0; i < nbRows; i++) {
                List<String> row = new ArrayList<>();
                for (int j=0 ; j<nbFields ; j++, idx++)
                    if (lsa.lvalue[idx] != 0)    //	is valid
                        row.add(lsa.svalue[idx]);
                    else
                        row.add(null);
                add(new MySqlRow(row));
            }
        }
        //===========================================================

    }
    //===============================================================
    //===============================================================

    //======================================================
    //======================================================
    private static class StringComparator implements Comparator<String> {
        public int compare(String s1, String s2) {
            return s1.compareToIgnoreCase(s2);
        }
    }
    //======================================================
    //======================================================
}
