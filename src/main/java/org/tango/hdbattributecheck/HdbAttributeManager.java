//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbattributecheck;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import org.tango.jhdb.HdbSigInfo;

import java.util.*;


/**
 * This class manage a set of HdbAttributes
 *
 * @author verdier
 */

public class HdbAttributeManager extends ArrayList<HdbAttribute> {
    private DevState state = DevState.UNKNOWN;
    private String status = "Not started";
    private List<String> statusList = new ArrayList<>();
    private List<DevState> stateList = new ArrayList<>();
    private HdbSigInfo[] hdbSigInfoArray;
    private ComputeGlobalThread thread;
    private final Object monitor = new Object();
    //===============================================================
    //===============================================================
    public HdbAttributeManager(String[] attributeNames, int checkPeriod, int archivePeriod) throws DevFailed {
        //  Get Context for all attributes
        Map<String, String[]> devContexts = getAttributeStrategyList();

        //  Build all HdbAttributes
        for (String attributeName : attributeNames) {
            String fullName = HdbAttribute.getFullName(attributeName);
            String[] contexts = devContexts.get(fullName);
            HdbAttribute hdbAttribute = new HdbAttribute(fullName, archivePeriod, contexts);
            add(hdbAttribute);
        }
        buildHdbSigInfoArray();

        thread = new ComputeGlobalThread(this, checkPeriod);
        thread.start();
    }
    //===============================================================
    //===============================================================
    private void buildHdbSigInfoArray() throws DevFailed {
        List<HdbSigInfo> sigInfoList = new ArrayList<>();
        for (HdbAttribute attribute : this) {
            if (attribute.isRegistered())
                sigInfoList.add(TangoUtils.getInstance().getHdbSigInfo(attribute.getFullName()));
        }
        hdbSigInfoArray = sigInfoList.toArray(new HdbSigInfo[0]);
    }
    //===============================================================
    //===============================================================
    private Map<String, String[]> getAttributeStrategyList() throws DevFailed {
        // Get managed attribute list
        List<String[]> devProperties = MySqlUtil.getDeviceProperties("sys/hdb-es/%", "AttributeList");

        Map<String, String[]> devContexts = new HashMap<>();
        for (String[] devProperty : devProperties) {
            // Get device name and strategy on property value
            StringTokenizer stk = new StringTokenizer(devProperty[1], ";");
            if (stk.countTokens()>=2) {
                String deviceName = stk.nextToken().trim();
                String strategy = stk.nextToken().trim();
                if (strategy.startsWith("strategy=")) {
                    strategy = strategy.substring("strategy=".length());
                }
                // Split contexts if several
                stk = new StringTokenizer(strategy, "|");
                List<String> contexts = new ArrayList<>();
                while (stk.hasMoreTokens())
                    contexts.add(stk.nextToken().trim());
                devContexts.put(deviceName, contexts.toArray(new String[0]));
            }
        }
        return devContexts;
    }
    //===============================================================
    //===============================================================
    public void setDisable(boolean[] disable) {
        int i=0;
        for (HdbAttribute attribute : this) {
            attribute.setDisable(disable[i++]);
        }
        // awake thread tor recompute state
        thread.wakeUp();
    }
    //===============================================================
    //===============================================================
    public DevState getState() {
        synchronized (monitor) {
            return state;
        }
    }
    //===============================================================
    //===============================================================
    public String getStatus() {
        synchronized (monitor) {
            return status;
        }
    }
    //===============================================================
    //===============================================================
    public DevState[] getStateList() {
        synchronized (monitor) {
            return stateList.toArray(new DevState[0]);
        }
    }
    //===============================================================
    //===============================================================
    public String[] getStatusList() {
        synchronized (monitor) {
            return statusList.toArray(new String[0]);
        }
    }
    //===============================================================
    //===============================================================
    private void setInformation(String status, DevState state, List<String> statusList, List<DevState> stateList) {
        synchronized (monitor) {
            this.state = state;
            this.status = status;
            this.stateList.clear();
            this.statusList.clear();
            this.stateList.addAll(stateList);
            this.statusList.addAll(statusList);
        }
    }
    //===============================================================
    //===============================================================



    //===============================================================
    //===============================================================
    private class ComputeGlobalThread extends Thread {
        private HdbAttributeManager manager;
        private int checkPeriod;
        private ComputeGlobalThread(HdbAttributeManager manager, int checkPeriod) {
            this.manager = manager;
            this.checkPeriod = checkPeriod;
        }
        @Override
        //===========================================================
        public void run() {
            // ToDo For test !!!
            if (System.getenv("TEST")!=null && System.getenv("TEST").equals("true"))
                checkPeriod = 15000;
            //  Build an array with attribute archive period
            List<Integer> list = new ArrayList<>();
            int maxPeriod = 0;
            for (HdbAttribute attribute : manager) {
                if (attribute.isRegistered()) {
                    int period = attribute.getArchivePeriod();
                    list.add(period);
                    if (period > maxPeriod) {
                        maxPeriod = period;
                    }
                }
            }
            int[] periods = new int[list.size()];
            for (int i=0 ; i<list.size() ; i++)
                periods[i] = list.get(i);

            while (true) {
                try {
                    //  Check all attributes, and dispatch results
                    long start = System.currentTimeMillis() - (maxPeriod + 5000); // A little bit more
                    boolean[] checked = TangoUtils.getInstance().checkEventsFromHdb(hdbSigInfoArray, start, periods);
                    int i=0;
                    for (HdbAttribute attribute : manager) {
                        // Only registered are read
                        if (attribute.isRegistered()) {
                            attribute.setHdbRead(checked[i++]);
                        }
                    }
                }
                catch (DevFailed e) {
                    for (HdbAttribute attribute : manager) {
                        attribute.setErrorMessage(e.errors[0].desc);
                        attribute.setState(DevState.FAULT);
                    }
                }

                // Build global state and status
                DevState state = DevState.ON;
                List<String> statusList = new ArrayList<>();
                List<DevState> stateList = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (HdbAttribute attribute : manager) {
                    if (!attribute.isDisable()) {
                        if (attribute.getState() == DevState.FAULT) {
                            state = DevState.FAULT;
                            sb.append(attribute.getFullName()).append(": ").
                                    append(attribute.getErrorMessage()).append('\n');
                        }
                        statusList.add(attribute.getErrorMessage());
                        stateList.add(attribute.getState());
                    }
                    else {
                        statusList.add("[DISABLE] " + attribute.getErrorMessage());
                        stateList.add(attribute.getState());
                    }
                }
                String status;
                if (state==DevState.FAULT)
                    status = "The device is FAULT\n" + sb.toString().trim();
                else
                    status = "All attributes OK";
                setInformation(status, state, statusList, stateList);
                waitNext(checkPeriod);
            }
        }
        //===========================================================
        private synchronized void wakeUp() {
            notify();
        }
        //===========================================================
        private synchronized void waitNext(int ms) {
            try {
                wait(ms);
            } catch (InterruptedException e) {
                /* */
            }
        }
        //===========================================================
    }
    //===============================================================
    //===============================================================
}
