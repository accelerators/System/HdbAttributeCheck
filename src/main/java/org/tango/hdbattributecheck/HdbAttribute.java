//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbattributecheck;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DbAttribute;

/**
 * This class models a TANGO attribute from HDB
 *
 * @author verdier
 */

public class HdbAttribute {
    private String fullName;
    private int archivePeriod;
    private String[] contexts;
    private DevState state = DevState.UNKNOWN;
    private String errorMessage = "";
    private boolean registered =true;
    private boolean disable = false;

    private final Object monitor = new Object();
    private static final String ATTRIBUTE_HEADER = "tango://";
    //===============================================================
    /**
     *
     * @param attributeName attribute name to control
     * @param archivePeriod shift in millis, to do not call HDB at same time
     * @throws DevFailed in case of database connection failed
     */
    //===============================================================
    public HdbAttribute(String attributeName, int archivePeriod, String[] contexts) throws DevFailed {
        this.fullName = getFullName(attributeName);
        this.archivePeriod = archivePeriod;
        this.contexts = contexts;
        if (contexts==null) {
            registered = false;
            setState(DevState.FAULT);
            setErrorMessage("Is not registered");
        }
        else {
            // If not set for HdbAttributeCheck device, get attribute property
            if (archivePeriod <= 0) {
                this.archivePeriod = readArchivePeriod();
            }
            // Is property found ?
            if (this.archivePeriod <= 0) {
                setState(DevState.FAULT);
                setErrorMessage("No archive period set");
            }
            System.out.println(fullName + ": " + this.archivePeriod + " ms");
        }
    }
    //===============================================================
    //===============================================================
    private int readArchivePeriod() throws DevFailed {
        //  Get archive period property
        AttributeProxy attributeProxy = new AttributeProxy(fullName);
        DbAttribute property = attributeProxy.get_property();
        if (property.is_empty("archive_period")) {
            return  0;
        }
        else {
            try {
                return Integer.parseInt(property.get_string_value("archive_period"));
            } catch (NumberFormatException e) {
                return 0;
            }
        }
    }
    //===============================================================
    //===============================================================
    public static String getFullName(String attributeName) throws DevFailed {
        if (attributeName.startsWith(ATTRIBUTE_HEADER))
            return attributeName;
        //  If tango host not in attribute name, add default one
        String tangoHost = TangoUtils.getTangoHost(ApiUtil.getTangoHost());
        return ATTRIBUTE_HEADER + tangoHost + "/" + attributeName;
    }
    //===============================================================
    //===============================================================
    public String getFullName() {
        return fullName;
    }
    //===============================================================
    //===============================================================
    public DevState getState() {
        synchronized (monitor) {
            if (state==DevState.FAULT || state==DevState.UNKNOWN)
                return DevState.FAULT;
            else
                return state;
        }
    }
    //===============================================================
    //===============================================================
    public void setState(DevState state) {
        synchronized (monitor) {
            this.state = state;
        }
    }
    //===============================================================
    //===============================================================
    public String getErrorMessage() {
        synchronized (monitor) {
            return errorMessage;
        }
    }
    //===============================================================
    //===============================================================
    public void setErrorMessage(String errorMessage) {
        synchronized (monitor) {
            this.errorMessage = errorMessage;
        }
    }
    //===============================================================
    //===============================================================
    public int getArchivePeriod() {
        return archivePeriod;
    }
    //===============================================================
    //===============================================================
    public boolean isRegistered() {
        return registered;
    }
    //===============================================================
    //===============================================================
    public void setHdbRead(boolean ok) {
        if (ok) {
            setErrorMessage("");
            setState(DevState.ON);
        } else {
            // Check if already in fault to do not overwrite error message
            if (errorMessage.isEmpty()) {
                setErrorMessage("Data missing in HDB");
                setState(DevState.FAULT);
            }
        }
    }
    //===============================================================
    //===============================================================
    public boolean isDisable() {
        return disable;
    }
    //===============================================================
    //===============================================================
    public void setDisable(boolean disable) {
        this.disable = disable;
    }
    //===============================================================
    //===============================================================
    @Override
    public String toString() {
        return fullName + ": " + ApiUtil.stateName(getState()) +
                (getErrorMessage().isEmpty()? "" : " -> " + getErrorMessage());
    }
    //===============================================================
    //===============================================================
}
