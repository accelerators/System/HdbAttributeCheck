//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbattributecheck;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoDs.Except;
import org.tango.jhdb.Hdb;
import org.tango.jhdb.HdbFailed;
import org.tango.jhdb.HdbReader;
import org.tango.jhdb.HdbSigInfo;
import org.tango.jhdb.data.HdbData;
import org.tango.jhdb.data.HdbDataSet;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * This class is a set of HDB and Tango utilities
 *
 * @author verdier
 */

public class TangoUtils {
    private Hdb hdb = null;
    private static final TangoUtils instance = new TangoUtils();
    private static final boolean trace =
            System.getenv("TRACE")!=null && System.getenv("TRACE").equals("true");
    //===============================================================
    //===============================================================
    public static TangoUtils getInstance() {
        return instance;
    }
    //===============================================================
    //===============================================================
    private String hdbTime(long t) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss");
        return simpleDateFormat.format(new Date(t));
    }
    //===============================================================
    //===============================================================
    public HdbSigInfo getHdbSigInfo(String attributeName) throws DevFailed {
        try {
            synchronized (instance) {
                if (hdb == null) {
                    hdb = new Hdb();
                    hdb.connect();
                }
            }
            return hdb.getReader().getSigInfo(attributeName);
        }
        catch (HdbFailed e) {
            e.printStackTrace();
            Except.throw_exception("HdbFailed", e.getMessage());
            return null;
        }
    }
    //===============================================================
    //===============================================================
    public void checkEventsFromHdb(String fullAttributeName, long start, int period) throws DevFailed {
        long end = System.currentTimeMillis();
        String strStart = hdbTime(start);
        String strEnd = hdbTime(end);
        System.out.println("Reading HDB++ between " + strStart + "  to  " + strEnd + " for " + fullAttributeName);
        // Use new extractor
        try {
            synchronized (instance) {
                if (hdb == null) {
                    hdb = new Hdb();
                    hdb.connect();
                }
            }
            HdbDataSet hdbDataSet = hdb.getReader().getData(fullAttributeName, strStart, strEnd);
            // Trace if requested
            if (trace) {
                for (int i = 0; i < hdbDataSet.size(); i++) {
                    HdbData hdbData = hdbDataSet.get(i);
                    long time = hdbData.getDataTime() / 1000; //  milliseconds is enough
                    String strTime = hdbTime(time);

                    if (hdbData.hasFailed()) {
                        System.err.println(strTime + ": [ERROR] " + hdbData.getErrorMessage());
                    } else {
                        System.out.println(strStart + ":  " + hdbData.getValueAsString());
                    }

                }
            }
            // compute expected values (-1 for intervals)
            int expected = (int)((end-start)/period)-1;
            if (hdbDataSet.size()<expected)
                Except.throw_exception("DataMissing", "Data missing in HDB");
        }
        catch (HdbFailed e) {
            e.printStackTrace();
            Except.throw_exception("HdbFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    public boolean[] checkEventsFromHdb(HdbSigInfo[] hdbSigInfoArray, long start, int[] periods) throws DevFailed {
        long end = System.currentTimeMillis();
        String strStart = hdbTime(start);
        String strEnd = hdbTime(end);
        System.out.println("Reading HDB++ between " + strStart + "  to  " + strEnd + " for attributes");
        // Use new extractor
        try {
            synchronized (instance) {
                if (hdb == null) {
                    hdb = new Hdb();
                    hdb.connect();
                }
            }

            HdbDataSet[] hdbDataSets = hdb.getReader().getData(hdbSigInfoArray, strStart, strEnd, HdbReader.MODE_NORMAL);
            // Trace if requested
            if (trace) {
                for (HdbDataSet hdbDataSet : hdbDataSets) {
                    for (int i = 0; i < hdbDataSet.size(); i++) {
                        HdbData hdbData = hdbDataSet.get(i);
                        long time = hdbData.getDataTime() / 1000; //  milliseconds is enough
                        String strTime = hdbTime(time);
                        String name = hdbDataSet.getName() + " (";
                        if (hdbData.hasFailed()) {
                            System.err.println(name + strTime + "): [ERROR] " + hdbData.getErrorMessage());
                        } else {
                            System.out.println(name + hdbData.timeToStr(hdbData.getDataTime()) + "):  " + hdbData.getValueAsString());
                        }
                    }
                }
            }
            boolean[] results = new boolean[hdbDataSets.length];
            int i=0;
            for (HdbDataSet hdbDataSet : hdbDataSets) {
                // compute expected values (-1 for intervals)
                if (hdbDataSet.size()==0 || periods[i]<=0) {
                    results[i++] = false;
                }
                else {
                    int expected = (int) ((end - start) / periods[i]) - 1;
                    results[i++] = hdbDataSet.size() >= expected;
                }
            }
            return results;
        }
        catch (HdbFailed e) {
            e.printStackTrace();
            Except.throw_exception("HdbFailed", e.getMessage());
            return null;
        }
    }
    //===============================================================
    //===============================================================


    //===============================================================
    //===============================================================
    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            System.err.println(e.toString());
        }

    }
    //===============================================================
    //===============================================================
    public static String getTangoHost(String tangoHost) throws DevFailed {

        //  Check syntax
        int p = tangoHost.indexOf('.');
        if (p<0)
            p = tangoHost.indexOf(':');
        if (p<0)
            Except.throw_exception("SyntaxError", "Bad syntax for TANGO_HOST (host:port)");

        //  Check if it is a TANGO_HOST
        String newTangoHost = ApiUtil.get_db_obj(tangoHost).get_tango_host();

        //  Check if alias
        String hostName = tangoHost.substring(0, p);
        if (newTangoHost.startsWith(hostName + '.'))
            return newTangoHost;    //  Not an alias
        else {
            //  substitute alias to real name
            p = newTangoHost.indexOf('.');
            if (p>0) {
                //  With FQDN, can add alias to FQDN
                return hostName + newTangoHost.substring(p);
            }
            else {
                try {
                    //  Get FQDN and append to alias name
                    String alias = InetAddress.getByName(hostName).getCanonicalHostName();
                    //System.out.println(hostName);
                    int idx = alias.indexOf('.');
                    String fqdn = "";
                    if (idx>0) {
                        fqdn = alias.substring(idx);
                    }
                    p = tangoHost.indexOf(':');
                    return hostName+fqdn+tangoHost.substring(p);
                }
                catch (UnknownHostException e) {
                    Except.throw_exception(e.toString(), e.toString());
                }
                return "";  //  Cannot occur
            }
        }
    }
    //===============================================================
    //===============================================================
}
